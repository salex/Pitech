package State;

import CoffeMaker.CoffeMakerAPI;

/**
 * Created by Sfat Alexandru on 20.12.2016.
 */
public class BoilWater implements IState {

    Integer waterTem;
    CoffeMakerAPI coffeMakerAPI = CoffeMakerAPI.getInstance();

    public BoilWater() {
        waterTem = 25;
        coffeMakerAPI.setBoilerState(0);
        coffeMakerAPI.setReliefValveState(1);
    }

    public IState nextState() {
        printMasajeState();

        if (stopBoil()) {
            return new Stop();
        }

        if (waterTem < 85) {
            waterTem += 10;
            return this;
        }

        return new Sprayed();
    }

    /*
    Buton pentru a opri fierberea
     */
    private boolean stopBoil() {
        return false;
    }

    private void printMasajeState(){
        System.out.println("");
        System.out.println("----------------------------BoilWater----------------------------");
        System.out.println("Setters:");
        System.out.println("boilerStatus = BOILER_ON");
        System.out.println("reliefValveState = VALVE_CLOSED");
        System.out.println("");
        System.out.println("Apa trebuie sa atinga o temperatura de peste 85 de grade");
        System.out.println("Momentam apa are:"+waterTem);
        System.out.println("------------------------------------------------------------");
    }
}
