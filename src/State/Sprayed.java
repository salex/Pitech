package State;

import CoffeMaker.CoffeMakerAPI;

/**
 * Created by Sfat Alexandru on 20.12.2016.
 */
public class Sprayed implements IState {
    CoffeMakerAPI coffeMakerAPI = CoffeMakerAPI.getInstance();

    public Sprayed() {
    }

    public IState nextState() {
        printMasajeState();

        if (noCup()) {
            return new Stop();
        }

        return new TurnCoffe();
    }

    private boolean noCup() {
        if (coffeMakerAPI.getWarmerPlateStatus() == 1) {
            return false;
        }
        return true;
    }

    private void printMasajeState(){
        System.out.println("");
        System.out.println("----------------------------Sprayed----------------------------");
        System.out.println("Getters:");
        System.out.println("WarmerPlateStatus - vefiricam daca exista un recipient in care sa turnam cafeaua");
        System.out.println("------------------------------------------------------------");
    }
}
