package State;

import CoffeMaker.CoffeMakerAPI;

/**
 * Created by Sfat Alexandru on 20.12.2016.
 */
public class TurnCoffe implements IState {

    CoffeMakerAPI coffeMakerAPI = CoffeMakerAPI.getInstance();

    public TurnCoffe() {
    }

    @Override
    public IState nextState() {
        printMasajeState();

        if(noCup()){
            return new Stop();
        }

        if(!cupIsNotFull()){
            return this;
        }

        return new CoffeeFinished();
    }

    private boolean noCup(){
        if(coffeMakerAPI.getWarmerPlateStatus()==1){
            return false;
        }
        return true;
    }

    private boolean cupIsNotFull(){
        return true;
    }

    private void printMasajeState(){
        System.out.println("");
        System.out.println("----------------------------TurnCoffe----------------------------");
        System.out.println("Getters:");
        System.out.println("getWarmerPlateStatus - turnam cafea cat timp cana nu este plina sau ramane pe pozitia ei");
        System.out.println("------------------------------------------------------------");
    }
}
