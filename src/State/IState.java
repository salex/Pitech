package State;

/**
 * Created by Sfat Alexandru on 20.12.2016.
 */
public interface IState {

    public Object nextState();

}
