package State;

import CoffeMaker.CoffeMakerAPI;

/**
 * Created by Sfat Alexandru on 20.12.2016.
 */
public class Start implements IState {

    CoffeMakerAPI coffeMakerAPI = CoffeMakerAPI.getInstance();

    public Start() {
    }

    public IState nextState() {
        printMasajeState();
        if (!startChecking()) {
            return new Stop();
        }

        return new BoilWater();
    }

    /*
    verificam daca exista apa suficienta
     */
    private boolean startChecking() {
        if(coffeMakerAPI.getBoilerStatus()==1){
            return true;
        }
        return false;
    }

    private void printMasajeState(){
        System.out.println("");
        System.out.println("----------------------------Start----------------------------");
        System.out.println("Getters:");
        System.out.println("getBoilerStatus - pentru a putea merge mai departe verificam daca avem suficienta apa si cafea, daca nu procesul se intrerupe");
        System.out.println("------------------------------------------------------------");
    }
}
