package State;

import CoffeMaker.CoffeMakerAPI;

/**
 * Created by Sfat Alexandru on 20.12.2016.
 */
public class Stop implements IState {

    CoffeMakerAPI coffeMakerAPI = CoffeMakerAPI.getInstance();

    public Stop() {
        coffeMakerAPI.setReliefValveState(0);
        coffeMakerAPI.setIndicatorState(1);
        coffeMakerAPI.setWarmerState(1);
        coffeMakerAPI.setBoilerState(1);
    }

    @Override
    public IState nextState() {
        printMasajeState();
        if (getBrewButtonStatus() == false) {
            return this;
        }
        return new Start();
    }

    public boolean getBrewButtonStatus(){
       coffeMakerAPI.getBrewButtonStatus();
        return true;
    }

    private void printMasajeState(){
        System.out.println("");
        System.out.println("----------------------------Stop----------------------------");
        System.out.println("Setters:");
        System.out.println("reliefValveState = VALVE_OPEN");
        System.out.println("indicatorState = INDICATOR_OFF");
        System.out.println("warmerState = WARMER_OFF");
        System.out.println("boilerStatus = BOILER_OFF");
        System.out.println("");
        System.out.println("Getters:");
        System.out.println("getBrewButtonStatus - daca e pornit putem incepe");
        System.out.println("------------------------------------------------------------");
    }
}
