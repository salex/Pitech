package State;

import CoffeMaker.CoffeMakerAPI;

/**
 * Created by Sfat Alexandru on 20.12.2016.
 */
public class CoffeeFinished implements IState {

    CoffeMakerAPI coffeMakerAPI = CoffeMakerAPI.getInstance();

    public CoffeeFinished() {
        coffeMakerAPI.setBoilerState(1);
        coffeMakerAPI.setIndicatorState(0);
        coffeMakerAPI.setReliefValveState(0);
        coffeMakerAPI.setWarmerState(0);
    }

    @Override
    public IState nextState() {
        printMasajeState();

        if (cupIsTaken()){
            return this;
        }

        return new Stop();
    }

    private boolean cupIsTaken(){
        int response = coffeMakerAPI.getWarmerPlateStatus();
        return false;
    }


    private void printMasajeState(){
        System.out.println("");
        System.out.println("----------------------------CoffeeFinished----------------------------");
        System.out.println("Setters:");
        System.out.println("setBoilerState = BOILER_OFF");
        System.out.println("indicatorState = INDICATOR_ON");
        System.out.println("reliefValveState = VALVE_OPEN");
        System.out.println("setWarmerState = WARMER_ON");
        System.out.println("");
        System.out.println("Getters:");
        System.out.println("getWarmerPlateStatus - mentinem starea aceasta, si temperatura cafelei cat timp recipientul ramane pe pozitia lui");
        System.out.println("------------------------------------------------------------");
    }
}
