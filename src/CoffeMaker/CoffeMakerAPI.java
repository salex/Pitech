package CoffeMaker;

/**
 * Created by Sfat Alexandru on 19.12.2016.
 */
public class CoffeMakerAPI implements ICoffeeMakerAPI {

    private int warmerPlateStatus = POT_EMPTY;
    private int boilerVolume = BOILER_NOT_EMPTY;
    private int brewButtonStatus = BREW_BUTTON_NOT_PUSHED;
    private int boilerStatus = BOILER_OFF;
    private int warmerState = WARMER_OFF;
    private int indicatorState = INDICATOR_OFF;
    private int reliefValveState = VALVE_CLOSED;

    private static CoffeMakerAPI coffeMakerAPI = null;

    private CoffeMakerAPI(){
    }

    public static CoffeMakerAPI getInstance(){
        if(coffeMakerAPI == null) {
            coffeMakerAPI = new CoffeMakerAPI();
        }
        return coffeMakerAPI;
    }


    public int getWarmerPlateStatus() {
        return this.warmerPlateStatus;
    }

    public int getBoilerStatus() {
        return this.boilerVolume;
    }

    public  int getBrewButtonStatus() {
        return this.brewButtonStatus;
    }

    public  void setBoilerState(int boilerStatus) {
        this.boilerStatus=boilerStatus;
    }

    public  void setWarmerState(int warmerState) {
        this.warmerState=warmerState;
    }

    public  void setIndicatorState(int indicatorState) {
        this.indicatorState=indicatorState;
    }

    public  void setReliefValveState(int reliefValveState) {
        this.reliefValveState = reliefValveState;
    }
}
