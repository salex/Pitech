package FinalStateMachine;

import CoffeMaker.CoffeMakerAPI;
import State.IState;
import State.Stop;

import java.util.concurrent.TimeUnit;

/**
 * Created by Sfat Alexandru on 19.12.2016.
 */
public class FSM {

    CoffeMakerAPI coffeMakerAPI = CoffeMakerAPI.getInstance();
    IState state = new Stop();

    public FSM() {
    }

    public void startBrew() throws InterruptedException {
        while (true) {
            state = (IState) state.nextState();
            TimeUnit.SECONDS.sleep(2);

            if(state instanceof Stop){
                System.out.println("\n\nEnd of program");
                break;
            }
        }
    }
}
